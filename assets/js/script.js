document.getElementById("btn__registrarse").addEventListener("click", register);//para q cuando haga click en el boton Registarse haga todo lo q puse en la funcion register
document.getElementById("btn__iniciar-sesion").addEventListener("click", iniciarSesion);//para q cuando haga click en el boton Iniciar sesion haga todo lo q puse en la funcion iniciarSesion

window.addEventListener("resize", anchoPagina);

//Declaracion de variables
var contenedor_login_register = document.querySelector(".contenedor__login-register");

var formulario_login = document.querySelector(".formulario__login"); //Con esto le digo q la variable formulario_login es igual a la clase formulario__login
var formulario_register = document.querySelector(".formulario__register");

var caja_trasera_login = document.querySelector(".caja__trasera-login");
var caja_trasera_register = document.querySelector(".caja__trasera-register");
//Esta funcion hace q cuando hagamos click en Registrarse muestre el formulario para hacerlo y oculte el de iniciar sesion

function register (){
    if (window.innerWidth > 850) {
        formulario_register.style.display = "block";//cuando le demos click al boton de registrarse, el formulario register va a tener un display block -> hace que el comportamiento del elemento sea como un bloque.
        contenedor_login_register.style.left = "390px"; //para q mueva los formularios 410 pixeles a la 
        formulario_login.style.display = "none"; //para que cuando me muestre el formulario de Register me oculte el de Login
        caja_trasera_register.style.opacity = "0";
        caja_trasera_login.style.opacity = "1";
    }else{
        formulario_register.style.display = "block";
        contenedor_login_register.style.left = "0px"; 
        formulario_login.style.display = "none"; 
        caja_trasera_register.style.display = "none";
        caja_trasera_login.style.display = "block";
        caja_trasera_login.style.opacity = "1";
    }
}
function iniciarSesion (){ //pongo todo lo contrario a la funcion register
    if (window.innerWidth > 850) {
        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "25px"; 
        formulario_login.style.display = "block"; 
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.opacity = "0";
    } else{
        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "0x"; 
        formulario_login.style.display = "block"; 
        caja_trasera_register.style.display = "block";
        caja_trasera_login.style.display = "none";
    }
}

function anchoPagina (){
    if (window.innerWidth > 850) {
        caja_trasera_login.style.display = "block";
        caja_trasera_register.style.display = "block";
    } else{
        caja_trasera_register.style.display = "block";
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.display = "none";
        formulario_login.style.display = "block";
        formulario_register.style.display = "none";
        contenedor_login_register.style.left = "0px";
    }
}
anchoPagina();