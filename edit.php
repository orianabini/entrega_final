<?php
    include('php/conexion_tablas.php');
    if (isset($_GET['id'])) {
        $id = $_GET['id']; 
        $query = "SELECT * FROM stock WHERE id = $id";
        $result = mysqli_query($conexion_tabla_stock, $query);
        if (mysqli_num_rows($result) == 1) { //para comprobar cuantas fils tiene mi rdo y en $result le paso el resultado
           $row = mysqli_fetch_array($result);
           $nombre = $row['Producto'];
           $descripcion = $row['Descripcion'];
           $color = $row['Color'];
           $talle = $row['Talle'];
           $precio = $row['Precio'];
        }
    }

    if (isset($_POST['actualizar'])) { //segun los nombres q le di al principio
        echo 'actualizando';
        $id = $_GET['id'];
        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
        $color = $_POST['color'];
        $talle = $_POST['talle'];
        $precio = $_POST['precio'];

        $query = "UPDATE stock SET Producto = '$nombre', Descripcion = '$descripcion', Color = '$color', Talle = '$talle', Precio = '$precio' WHERE id = $id";//el id ni va con comillas xq es numerico
        mysqli_query($conexion_tabla_stock, $query);

        $_SESSION ['message'] = 'Producto actualizado exitosamente';
        $_SESSION ['message_type'] = 'success';

        header("Location: index2.php");
    }
?>
<?php include("includes/header.php")?>
<!--clases de bootstrap-->
<div class = "container p-4"> <!--padding de 4-->
    <div class = "row">
        <div class="col-md-4 mx-auto"> <!--mi formulario va a medir 4 columnas y para q esté centradoi pongo mx-auto-->
            <div class="card card-body">
                <form action="edit.php?id=<?php echo $_GET['id']; ?>" method = "POST">
                    <div class="form-group">
                        <input type="text" name = "nombre" value = "<?php echo $nombre;?>"
                        class = "form-control" placeholder = "Actualice el nombre">
                    </div>
                    <div class="form-group">
                    <textarea name="descripcion" rows="2" class = "form-control" placeholder = "Actualice la descripcion"><?php echo $descripcion;?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" name = "color" value = "<?php echo $color;?>"
                        class = "form-control" placeholder = "Actualice el color">
                    </div>
                    <div class="form-group">
                        <input type="text" name = "talle" value = "<?php echo $talle;?>"
                        class = "form-control" placeholder = "Actualice el talle">
                    </div>
                    <div class="form-group">
                        <input type="text" name = "precio" value = "<?php echo $precio;?>"
                        class = "form-control" placeholder = "Actualice el precio">
                    </div>
                    <button class = " btn btn-primary" name = "actualizar">
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    </div>

</div>

<?php include("includes/footer.php")?>