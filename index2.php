<?php include("php/conexion_tablas.php")?> <!--Aca estamos poniendo q la clase index2 hereda lo de db-->
<?php include("includes/header.php")?> <!--Aca estamos poniendo q la clase index2 hereda lo de header-->
<div class = "container p-5"> <!--agrego un contenedor de bootstrap q me permite centrar el contenido, 
                                o sea le agrega espacio en todos los aldos y un padding de 5 para q el contenido se vea bien.
                                O sea, agrega margenes arriba y a la izquiera. Va del 1 al 5.-->
<div class = "row"> <!--me pone todo lo q viene en la primiera mitad de la pag, no de punta a punta-->
        <div class="col md-4"><!--VER-->

            <?php if (isset($_SESSION['message'])) { ?> <!--si existe mi variable message dentro de la session significa q hay un mensaje q quiere mostrar. O sea, aca compruebo si existe-->
            <!--VER COMO PONER LA X EN LA PUNTA-->     
                <!--Bootstrap de aca-->
                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </symbol>
                </svg>
                <div class="alert alert-<?= $_SESSION['message_type'] ?>
                d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="20" height="20" role="img" aria-label="Success:">
                        <use xlink:href="#check-circle-fill"/>
                    </svg>
                <!--Bootstrap hasta aca-->
                    <?= $_SESSION ['message'] ?><!--aca lo muestro-->
                    <!--Bootstrap de aca-->
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <!--Bootstrap hasta-->
                </div>

            <?php session_unset(); } ?> <!--aca eliminamos las variables q guardé en session para q cuando refresque la pag me deje de aparecer el mensaje-->

            <div class="card card-body"> <!--tarjeta, adentro creo un formulario-->         
                <!--formulario-->
                <form action="php/registro_producto.php" method = "POST"> <!--form action: cuando hago click en "Guardar producto" tiene q enviar lo q inserté en los campos al archivo "php/registro_producto.php" a traves del metodo POST-->
                    <div class="form-group"> <!-- para q separe los input -->
                        <input type="text" name = "nombre" class = "form-control" placeholder = "Producto" autofocus> 
                        <!--input type text: para poner un espacio donde se inserten datos tipo texto. Placeholder para indicar lo q hay q poner en ese campo-->
                    </div>
                    <div class="form-group"> <!-- para q separe los input -->
                        <textarea name = "descripcion" rows="3" class = "form-control" placeholder = "Descripcion del producto"></textarea>                 
                        <!--textarea: es como el input pero con un espacio mas grande para insertar datos. 
                        rows: para determinar las filas (renglones) del campo. id: por si tengo q hacer algo en java script-->
                    </div>
                    <div class="form-group"> <!-- para q separe los input -->
                        <input type="text" name = "color" class = "form-control" placeholder = "Color" autofocus> 
                    </div>
                    <div class="form-group"> <!-- para q separe los input -->
                        <input type="text" name = "talle" class = "form-control" placeholder = "Talle" autofocus> 
                    </div>
                    <div class="form-group"> <!-- para q separe los input -->
                        <input type="text" name = "precio" class = "form-control" placeholder = "Precio" autofocus> 
                    </div>
                        <input type="submit" class = "btn btn-success btn-block" name = "save" value = "Guardar producto"> 
                    <!--input de tipo sumbit para que cuando yo de click en este input ejecute el formulario--> 
                    <!--btn-block: para q ocupe todo el anchi de la tarjeta-->
                </form>
            </div>
        </div>
        <div class="col md-8"><!--VER-->
            <table class = "table table-bordered"> <!--para crear una tabla-->
                <thead> <!--encabezado de la tabla-->
                    <th>Nombre del producto</th> <!--<th> define una celda como encabezado de un grupo de celdas en una tabla-->
                    <th>Descripcion</th>
                    <th>Color</th>
                    <th>Talle</th>
                    <th>Precio</th>
                </thead>
                <tbody> <!--cuerpo de la tabla-->
                    <?php
                    $query = "SELECT * FROM stock"; //aca le pido todas los productos q vienen de la tabla stock
                    $result_stock = mysqli_query ($conexion_tabla_stock, $query) //me devueve como rdo todos los productos
                    ?>

                    <?php while ($row = mysqli_fetch_array($result_stock)) {  ?> <!--esto recorre mis productos-->
                        <tr> <!--fila-->
                            <td> <?php echo $row ['Producto'] ?> </td> <!--celdas por fila   . A traves de php mostrame desde row/fila el valor de la columna Producto q esta en mi tabla stock-->
                            <td> <?php echo $row ['Descripcion'] ?></td>
                            <td> <?php echo $row ['Color'] ?> </td>
                            <td> <?php echo $row ['Talle'] ?> </td>
                            <td> <?php echo $row ['Precio'] ?> </td>
                            <td> 
                                <a href = "edit.php?id=<?php echo $row['id'] ?>" class = "btn btn-primary">
                                    <i class="fas fa-edit"></i></a>
                                <a href = "delete.php?id=<?php echo $row['id'] ?>" class = "btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                        
                    <?php } ?>



                </tbody>
            </table>
            <form action="php/registro_pedido.php">
                <input type="submit" class = "btn btn-success btn-block" name = "pedir" value = "">
            </form>
        </div>

    </div>



</div>

<?php include("includes/footer.php")?> <!--Aca estamos poniendo q la clase index2 hereda lo de footer-->